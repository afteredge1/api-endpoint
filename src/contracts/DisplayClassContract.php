<?php

namespace TDS\contracts;

interface DisplayClassContract
{
    public function formatData();
}
