<?php

namespace TDS;

class APIClass
{
    protected $data;
    public function __construct()
    {
    }

    public function fetchData()
    {

        $curl = curl_init();

        try {
            curl_setopt_array($curl, array(
                CURLOPT_URL => 'https://postman-echo.com/get?foo1=bar1&foo2=bar2',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'GET',
            ));

            $this->data = curl_exec($curl);
        } catch (\Throwable $th) {
            throw $th;
        }
        curl_close($curl);

        return $this->data;
    }
}
