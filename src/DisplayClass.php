<?php

namespace TDS;

use TDS\APIClass;
use TDS\contracts\DisplayClassContract;

class DisplayClass implements DisplayClassContract
{
    protected $api;
    public function __construct()
    {
        $this->api = new APIClass();
    }

    public function formatData()
    {
        $data = $this->api->fetchData();
        $args = json_decode($data, true);
        return $args['args'];
    }
}
